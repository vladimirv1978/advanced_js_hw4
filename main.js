let container = document.querySelector(".container");
let cardFilm;
let personages = [];


class Movie {
  constructor(episodeId, name, openingCrawl, characters) {
    this.episodeId = episodeId;
    this.name = name;
    this.openingCrawl = openingCrawl;
    this.characters = characters;
  }

  renderFilm() {
    let datafilm = [this.episodeId, this.name, this.openingCrawl];

     cardFilm = document.createElement("div");
    container.append(cardFilm);

    datafilm.forEach((elem) => {
      const filmElement = document.createElement("h3");
      filmElement.innerText = `${elem}`;
      cardFilm.append(filmElement);
    });
  }

  renderCharacters() {
    this.characters.forEach((elem) => {
      const charactersElement = document.createElement("h4");
      charactersElement.innerText = `${elem}`;
      cardFilm.lastChild.before(charactersElement);
    });
  }
}

function getMovieData(url) {
  fetch(url)
    .then((res) => res.json())
    .then((data) => {

      for (let i = 0; i <= data.length - 1; i++) {
        let { episodeId, name, openingCrawl, characters } = data[i];

        let listCharacters = characters.map((character) => fetch(character));
        Promise.all(listCharacters)
          .then((responses) => Promise.all(responses.map((r) => r.json())))
          .then((users) => {
            personages = [];
            for (let elem of users) {
              personages.push(elem.name);
            }
            let movieCard = new Movie(episodeId, name, openingCrawl, personages);
            movieCard.renderFilm();
            movieCard.renderCharacters();
          });
      }
    });
}

getMovieData("https://ajax.test-danit.com/api/swapi/films");
